﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace HuffmanTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.WriteLine("Будь ласка, введіть речення або текст:");
            string input = Console.ReadLine();
            HuffmanTree huffmanTree = new HuffmanTree();

            // Будування дерева
            huffmanTree.Build(input);

            // Закодовування
            BitArray encoded = huffmanTree.Encode(input);

            Console.Write("Закодовано: ");
            foreach (bool bit in encoded)
            {
                Console.Write((bit ? 1 : 0) + "");
            }
            Console.WriteLine();

            // Розшифрування
            string decoded = huffmanTree.Decode(encoded);

            Console.WriteLine("Розшифровано: " + decoded);

            Console.ReadLine();
        }
    }
}